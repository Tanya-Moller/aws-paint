# Setting up AWS machine EC2 with paint

In this project, we will make a bash script to provision a machine in aws to run paint

## Subjects covered
---

This project will help us cover:
- Git 
- Bitbucket
- Markdown
- AWS 
    1. EC2 creation
    2. AWS Security Groups
- Redhat
    1. Installation Process
    2. Scripts
- Bash
    1. ssh
    2. scp
    3. heredoc
    4. Outputs (stdin, stdout, stderr)
    5. Piping and grep


##  Installing jspaint onto AWS Webserver
---
### Thought process:
- Get the code
- Get the relevant code compiler (e.g. JS package manager)
- Install the dependencies 
- Start the server (init script)
  
**1. Code to Load the Server and Install apache (http server)**

```bash
$ #!/bin/bash

$ if (( $#  > 0 ))
$ then
    hostname=$1 
$ else
    echo "WTF: you must supply a hostname or IP address" 1>&2 
    exit 1
$ fi 
```
- Evaluates a condition (returns true/false/binary) - if the number of arguments is greater than 0
- Then make the first argument hostname
- 1>&2 Move and merge stdout to stderr
- Non-zero value makes it identifiable as an error

```bash
$ ssh -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem $ ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
else
    exit 1
fi'
```
- `ssh -o StrictHostKeyChecking=no` stops it asking when adding new keys
- `-i` is for public key authentication
- if is looking at the exit status of the last command in the command loop
  
**2. Install git**

```bash
$ sudo yum -y install git
```

**3. Clone git file of jspaint and open the directory**

```bash
$ git clone https://github.com/1j01/jspaint.git
$ cd jspaint
```

**4. Install Node.js via binary archive on Linux**
- Instructions from https://github.com/nodejs/help/wiki/Installation#how-to-install-nodejs-via-binary-archive-on-linux

```bash

$ VERSION=v14.16.1
$ DISTRO=linux-x64
$ sudo mkdir -p /usr/local/lib/nodejs
$ wget https://nodejs.org/dist/v14.16.1/node-v14.16.1-linux-x64.tar.xz
$ sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

$ echo '
# Nodejs
VERSION=v10.15.0
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH' >>/.bash_profile
$ source ~/.bash_profile
$ npm i
```
- make the variables `VERSION` and `DISTRO` (distribution)
- make a directory for nodejs. The `-p` means that if the parent path doesn't exist, create it
- `wget` retrieves the content from the web server specified
- `sudo tar -xJvf` extracts the archive after it
- The PATH is the path the terminal takes to find node (so it allows you to use short name e.g. node)
    - Remember the example with the VSCODE path and `code`
- The bin is part of the new path
- `:$PATH` is at the end so you add all the original path stuff to the end before adding this new path --> because the right hand side of the = will be dealt with first.
- Note: if `$PATH:` was at the beginning, just after the `=` it would read the node commands first (this would also slow down normal path as the node is first)
- `:` is used for separating
- Then append this to `/.bash_profile`
- `npm -i` installs the dependencies from the JavaScript package manager npm - must be in the directory to install the dependencies (we did `cd jspaint` earlier). You have to set the path before you run it.


**5. Creating a reverse proxy**
- https://www.digitalocean.com/community/tutorials/how-to-use-apache-as-a-reverse-proxy-with-mod_proxy-on-centos-7

```bash
$ cd /etc/httpd/conf.d
$ sudo sh -c "echo \"<VirtualHost *:80>
    ProxyPreserveHost On

    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" > jspaint.conf"
$ sudo systemctl restart httpd
$ cd jspaint/
```
- enter the `conf.d` directory and make a conf file for jspaint
- 80 is the 'main' gate' or public entrance
- Then we write into a file called jspaint.conf using `sudo sh -c` (shell command - non-interactive mode). Launching a shell and telling the shell to run the command in quotes
  - can't just sudo echo because of the redirection at the end. So because we intend to output the file with redirection, we have to tell sudo to run a shell script because the redirection has to be part of the sudo. the sh -c is creating a subshell.
  - If the redirection happens before the sudo, the elevated user won't have access so it will fail. we are delaying the redirection by putting it in a string with the commands.
- `<VirtualHost>` This is a tag (XML) - within this tag, there are the properties. Contains directives(instructions) that apply only to a specific hostname or IP address
  - Listen to any IP on port 80 and pass them on to 127.0.0.1:8080
- Set up the :8080 proxy for any IP ending in :80 and insert it into the `jspaint.conf` file.
- Need the `\\"` so that it recognizes the inside quotes

**6. Run node**

```bash
$ npm run dev
```


#### Start/stop script for a service

```bash
$ sudo sh -c "cat > /etc/init.d/jspaint <<_END

#!/bin/bash
#description: JSPaint web app
#chkconfig: 2345 99 99
case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
esac 
_END"
```

- The `#` is a special comment for the `chkconfig` command to set up the running specifications (see below).
- `#chkconfig 2345 99 99` - this makes the init available to run at level 2,3,4,5, the service will start when the system is booted. 
  - The first 99 is start priority, so it is the last thing to start.
  - The 2nd 99 is stop priority, so it is the first thing to stop.
- `sudo sh -c` - execute the shell script in current shell.
- `<` is opening a file, `<<` is opening an end of input marker (the `_END`) (Telling it to get the input between those markers).
  - Opens on the right to put to the cat to the left to the file in the middle.
  - `<` means shell opens up the file and sends the output as input?
- `_END` indicates a heredoc.
- `case` is defining the possible options (it's like an if function).
  - it is part of the shell.
- `;;` prevents fall-through (like a hard stop).
- `nohup` is no hangup - it keeps the command running, even if it is not associated to a user.
- `&` at the end means run in background.
- `egrep` is extended grep - put it in quotes.
  - `|` to separate (it is an 'or' in this case).
- `ps -ef` lists current processes - then we `grep` for npm and node, then print the PID using `awk '{print $2}` so `kill -9` kills the process.
  - `kill -9` - normal `kill` terminates the process, the `-9` makes it 'aggressive' (last resort).
- `awk` is good for extracting columns - works out columns based on the next non-space character (tabs or space).
  - `''` prevents it fetching the input from the command line.
  - `{}` tells awk to do it for every line of input - have to do this for `awk`.
- `esac` ends the case statement.

#### If you create a separate file called /etc/init.d/jspaint.init it changes like this:

```bash
$ #!/bin/bash
$ #description: JSPaint web app
$ #chkconfig: 2345 99 99
$ PATH=$PATH
$ case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
$ esac 
```
and the original file changes like this:

```bash
$ sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin," /etc/init.d/jspaint

$ sudo chkconfig --add jspaint
$ sudo systemctl enable jspaint
$ sudo /etc/init.d/jspaint start
```

#### Change permissions
```
$ sudo chmod +x /etc/init.d/jspaint
$ sudo chkconfig --add jspaint
$ sudo systemctl enable jspaint
$ sudo systemctl start jspaint
```
- `chkconfig --add jspaint` adds the new service we just made in that file (so we can use jspaint as a service)
  - adds it to the startup sequence
- then we `enable` (make sure the system knows about it) and `start` it using `systemctl`

## Notes:

check the right version is installed 

```
$ node -v
```
Inbound rules:
- 80 --->from anywhere 
- 22 --> your ip

Redirection heredoc example:
```bash
$ grep 'root' <<_END_
hello
goodbye
I'll get to the root of this
_END_
```
> I\'ll get to the **root** of this

#### Secure copy the init file to the webserver

```bash
$ scp -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem jspaint.init ec2-user@$hostname:jspaint.init
```

#### Move the jspaint file from home to /etc/init.d and rename it jspaint


```bash
$ sudo mv ~/jspaint.init /etc/init.d/jspaint
```

#### Change the permissions

```bash
$ sudo chmod +x /etc/init.d/jspaint
```

#### Check web processes running

```bash
$ netstat -tln
$ netstat -tlnp
```

#### Find and replace

```bash
$ sed 's,<what you want to find>,<what you want to replace it with>,' <path>
# Example:
$ sed 's,nohup npm,nohup /usr/bin/npm,' /etc/init.d/jspaint
```
- Commas separate the stuff you're looking for and finding

```bash
$ sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin," /etc/init.d/jspaint
```
- the `-i` (not in mac though) makes it change it permanently
- The colon `:` separates the directories in `PATH`